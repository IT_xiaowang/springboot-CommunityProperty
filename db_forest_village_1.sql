/*
 Navicat Premium Data Transfer

 Source Server         : wanghuan
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : db_forest_village

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 31/10/2020 16:36:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for forest_cararea
-- ----------------------------
DROP TABLE IF EXISTS `forest_cararea`;
CREATE TABLE `forest_cararea`  (
  `carAreaID` int(11) NOT NULL AUTO_INCREMENT COMMENT '车辆关联编号',
  `carID` int(11) NULL DEFAULT NULL COMMENT '车位编号',
  `yeZhuID` int(11) NULL DEFAULT NULL COMMENT '业主编号',
  `typeID` int(11) NULL DEFAULT NULL COMMENT '车位编号',
  `areaTypeID` int(11) NULL DEFAULT NULL COMMENT '车位区域编号',
  `carDate` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车位时间',
  PRIMARY KEY (`carAreaID`) USING BTREE,
  INDEX `carID`(`carID`) USING BTREE,
  INDEX `typeID`(`typeID`) USING BTREE,
  INDEX `yeZhuID`(`yeZhuID`) USING BTREE,
  INDEX `areaTypeID`(`areaTypeID`) USING BTREE,
  CONSTRAINT `carID` FOREIGN KEY (`carID`) REFERENCES `forest_carmessage` (`carid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `typeID` FOREIGN KEY (`typeID`) REFERENCES `forest_cartype` (`typeID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `yeZhuID` FOREIGN KEY (`yeZhuID`) REFERENCES `forest_yezhumessage` (`yezhuid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `areaTypeID` FOREIGN KEY (`areaTypeID`) REFERENCES `forest_carareatype` (`carareatypeid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_cararea
-- ----------------------------
INSERT INTO `forest_cararea` VALUES (1, 1, 1, 1, 1, '2020-10-24 16:35:15');
INSERT INTO `forest_cararea` VALUES (2, 2, 5, 2, 2, '2020-10-25 16:35:15');

-- ----------------------------
-- Table structure for forest_carareatype
-- ----------------------------
DROP TABLE IF EXISTS `forest_carareatype`;
CREATE TABLE `forest_carareatype`  (
  `carAreaTypeID` int(11) NOT NULL AUTO_INCREMENT COMMENT '车位区域编号',
  `carAreaTypeName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车位区域名称',
  `carAreaTypeDate` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车位区域时间',
  `carAreaTypeBeiZhu` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车位区域备注',
  PRIMARY KEY (`carAreaTypeID`) USING BTREE,
  INDEX `carAreaTypeID`(`carAreaTypeID`) USING BTREE,
  INDEX `carAreaTypeID_2`(`carAreaTypeID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_carareatype
-- ----------------------------
INSERT INTO `forest_carareatype` VALUES (1, 'A区', '2020-10-24 16:35:15', NULL);
INSERT INTO `forest_carareatype` VALUES (2, 'B区', '2020-10-24 16:35:16', NULL);

-- ----------------------------
-- Table structure for forest_carmessage
-- ----------------------------
DROP TABLE IF EXISTS `forest_carmessage`;
CREATE TABLE `forest_carmessage`  (
  `carID` int(11) NOT NULL AUTO_INCREMENT COMMENT '车位ID',
  `carImage` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车辆照片',
  `carPlates` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车牌照',
  `carStates` int(255) NULL DEFAULT NULL COMMENT '车位状态(\r\n2 车位未售出\r\n1  车位已售出)',
  `catDate` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车位时间',
  `carBeiZhu` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`carID`) USING BTREE,
  INDEX `yeZhuID_id`(`catDate`) USING BTREE,
  INDEX `carID`(`carID`) USING BTREE,
  INDEX `carID_2`(`carID`) USING BTREE,
  INDEX `carID_3`(`carID`) USING BTREE,
  INDEX `carID_4`(`carID`) USING BTREE,
  INDEX `carID_5`(`carID`) USING BTREE,
  INDEX `carID_6`(`carID`) USING BTREE,
  INDEX `carID_7`(`carID`) USING BTREE,
  INDEX `carID_8`(`carID`) USING BTREE,
  INDEX `carID_9`(`carID`) USING BTREE,
  INDEX `carID_10`(`carID`) USING BTREE,
  INDEX `carID_11`(`carID`) USING BTREE,
  INDEX `carID_12`(`carID`) USING BTREE,
  INDEX `carID_13`(`carID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_carmessage
-- ----------------------------
INSERT INTO `forest_carmessage` VALUES (1, '1.jpg', '豫666966', 1, '2020-10-25 16:25:04', NULL);
INSERT INTO `forest_carmessage` VALUES (2, '11.jpg', '豫222222', 2, '2020-10-25 16:25:06', NULL);

-- ----------------------------
-- Table structure for forest_cartype
-- ----------------------------
DROP TABLE IF EXISTS `forest_cartype`;
CREATE TABLE `forest_cartype`  (
  `typeID` int(11) NOT NULL AUTO_INCREMENT COMMENT '车位编号',
  `cartTypeName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车位名称',
  `carTypeDate` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车位时间',
  `carTypeBeiZhu` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车位备注',
  PRIMARY KEY (`typeID`) USING BTREE,
  INDEX `cartypeID`(`typeID`) USING BTREE,
  INDEX `typeID`(`typeID`) USING BTREE,
  INDEX `typeID_2`(`typeID`) USING BTREE,
  INDEX `typeID_3`(`typeID`) USING BTREE,
  INDEX `typeID_4`(`typeID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_cartype
-- ----------------------------
INSERT INTO `forest_cartype` VALUES (1, '1-1', '2020-10-24 16:25:04', NULL);
INSERT INTO `forest_cartype` VALUES (2, '1-2', '2020-10-24 16:25:05', NULL);
INSERT INTO `forest_cartype` VALUES (3, '1-3', '2020-10-24 16:25:34', NULL);
INSERT INTO `forest_cartype` VALUES (4, '10-1', '2020-10-24 16:25:45', NULL);
INSERT INTO `forest_cartype` VALUES (5, '10-2', '2020-10-25 16:25:04', NULL);
INSERT INTO `forest_cartype` VALUES (6, '10-3', '2020-10-25 16:25:04', NULL);

-- ----------------------------
-- Table structure for forest_complaint
-- ----------------------------
DROP TABLE IF EXISTS `forest_complaint`;
CREATE TABLE `forest_complaint`  (
  `comID` int(11) NOT NULL AUTO_INCREMENT COMMENT '投诉ID',
  `yeZhuID` int(11) NULL DEFAULT NULL COMMENT '业主ID',
  `roomNameID` int(11) NULL DEFAULT NULL COMMENT '房间ID',
  `comTitle` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '投诉标题',
  `comContent` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '投诉内容',
  `comMatter` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '投诉类别',
  `comState` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '处理状态\r\n(0 未处理\r\n1 正在处理\r\n2 处理完成)',
  `comAssess` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '住户评价',
  `comDate` datetime(0) NULL DEFAULT NULL COMMENT '投诉时间',
  PRIMARY KEY (`comID`) USING BTREE,
  INDEX `FOREST_Complaint_id1`(`yeZhuID`) USING BTREE,
  INDEX `FOREST_Complaint_id2`(`roomNameID`) USING BTREE,
  CONSTRAINT `FOREST_Complaint_id1` FOREIGN KEY (`yeZhuID`) REFERENCES `forest_yezhumessage` (`yezhuid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FOREST_Complaint_id2` FOREIGN KEY (`roomNameID`) REFERENCES `forest_roomnamemessage` (`roomnameid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_complaint
-- ----------------------------

-- ----------------------------
-- Table structure for forest_currententry
-- ----------------------------
DROP TABLE IF EXISTS `forest_currententry`;
CREATE TABLE `forest_currententry`  (
  `currentEntryID` int(11) NOT NULL AUTO_INCREMENT COMMENT '日常记录ID',
  `currentEntryName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '日常名称（姓名）',
  `xtYongHuID` int(11) NULL DEFAULT NULL COMMENT '物业ID',
  `yeZhuID` int(11) NULL DEFAULT NULL COMMENT '业主ID',
  `styleID` int(11) NULL DEFAULT NULL COMMENT '类型ID',
  `currentEntryBeiZhu` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`currentEntryID`) USING BTREE,
  INDEX `FOREST_currentry_id1`(`xtYongHuID`) USING BTREE,
  INDEX `FOREST_currentry_id2`(`yeZhuID`) USING BTREE,
  INDEX `FOREST_currentry_id3`(`styleID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_currententry
-- ----------------------------
INSERT INTO `forest_currententry` VALUES (1, 'wanghuan1', 2, 1, 1, NULL);
INSERT INTO `forest_currententry` VALUES (3, 'wanghuan1', 1, 2, 1, NULL);
INSERT INTO `forest_currententry` VALUES (4, 'wanghuan1', 1, 1, 2, NULL);
INSERT INTO `forest_currententry` VALUES (5, 'wanghuan1', 2, 46, 3, NULL);
INSERT INTO `forest_currententry` VALUES (6, 'wanghuan1', 2, 47, 1, NULL);
INSERT INTO `forest_currententry` VALUES (7, 'wanghuan1', 2, 48, 1, NULL);
INSERT INTO `forest_currententry` VALUES (8, 'wanghuan1', 2, 49, 1, NULL);
INSERT INTO `forest_currententry` VALUES (10, 'wanghuan1', 2, 1, 3, NULL);
INSERT INTO `forest_currententry` VALUES (11, 'wanghuan1', 2, 8, 1, NULL);
INSERT INTO `forest_currententry` VALUES (12, 'wanghuan1', 2, 8, 1, NULL);

-- ----------------------------
-- Table structure for forest_currententrystyle
-- ----------------------------
DROP TABLE IF EXISTS `forest_currententrystyle`;
CREATE TABLE `forest_currententrystyle`  (
  `styleID` int(11) NOT NULL AUTO_INCREMENT COMMENT '类型ID',
  `styleName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '操作类型',
  `styleBeiZhu` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`styleID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_currententrystyle
-- ----------------------------
INSERT INTO `forest_currententrystyle` VALUES (1, '新增信息', NULL);
INSERT INTO `forest_currententrystyle` VALUES (2, '修改信息', NULL);
INSERT INTO `forest_currententrystyle` VALUES (3, '删除信息', NULL);
INSERT INTO `forest_currententrystyle` VALUES (4, '查询信息', NULL);

-- ----------------------------
-- Table structure for forest_ordermanage
-- ----------------------------
DROP TABLE IF EXISTS `forest_ordermanage`;
CREATE TABLE `forest_ordermanage`  (
  `orderID` int(11) NOT NULL AUTO_INCREMENT COMMENT '收费单ID',
  `projectID` int(11) NOT NULL COMMENT '收费项ID',
  `orderCharge` double NULL DEFAULT NULL COMMENT '单价',
  `orderChargeActual` double NULL DEFAULT NULL COMMENT '收费价格',
  `orderState` int(255) NULL DEFAULT NULL COMMENT '	收费状态（\r\n1、已收费\r\n0、未收费\r\n）',
  `orderDate` datetime(0) NULL DEFAULT NULL COMMENT '订单时间',
  `yeZhuID` int(11) NULL DEFAULT NULL COMMENT '业主ID',
  `roomNameID` int(11) NULL DEFAULT NULL COMMENT '关联ID',
  PRIMARY KEY (`orderID`) USING BTREE,
  INDEX `forest_ordermessage01`(`projectID`) USING BTREE,
  INDEX `forest_ordermessage02`(`yeZhuID`) USING BTREE,
  INDEX `forest_ordermessage03`(`roomNameID`) USING BTREE,
  CONSTRAINT `forest_ordermessage01` FOREIGN KEY (`projectID`) REFERENCES `forest_projectmanage` (`projectid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `forest_ordermessage02` FOREIGN KEY (`yeZhuID`) REFERENCES `forest_yezhumessage` (`yezhuid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `forest_ordermessage03` FOREIGN KEY (`roomNameID`) REFERENCES `forest_roomnamemessage` (`roomnameid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_ordermanage
-- ----------------------------

-- ----------------------------
-- Table structure for forest_projectmanage
-- ----------------------------
DROP TABLE IF EXISTS `forest_projectmanage`;
CREATE TABLE `forest_projectmanage`  (
  `projectID` int(11) NOT NULL AUTO_INCREMENT COMMENT '收费项ID',
  `projectName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '收费项名称',
  `projectPrice` decimal(10, 2) NULL DEFAULT NULL COMMENT '单价',
  PRIMARY KEY (`projectID`) USING BTREE,
  INDEX `ProjectID`(`projectID`) USING BTREE,
  INDEX `ProjectID_2`(`projectID`) USING BTREE,
  INDEX `ProjectID_3`(`projectID`) USING BTREE,
  INDEX `ProjectID_4`(`projectID`) USING BTREE,
  INDEX `ProjectID_5`(`projectID`) USING BTREE,
  INDEX `ProjectID_6`(`projectID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_projectmanage
-- ----------------------------

-- ----------------------------
-- Table structure for forest_repairrequest
-- ----------------------------
DROP TABLE IF EXISTS `forest_repairrequest`;
CREATE TABLE `forest_repairrequest`  (
  `repairID` int(11) NOT NULL AUTO_INCREMENT COMMENT '维修ID',
  `yeZhuID` int(11) NULL DEFAULT NULL COMMENT '业主ID',
  `shenQingShiJian` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '申请时间',
  `repairMessage` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '维修项目',
  `repairState` int(11) NULL DEFAULT NULL COMMENT '维修状态\r\n(0 未处理\r\n1 正在处理\r\n2 处理完成)',
  `repairDetail` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '维修详情',
  `roomNameID` int(11) NULL DEFAULT NULL COMMENT '关联ID',
  PRIMARY KEY (`repairID`) USING BTREE,
  INDEX `FOREST_RepairRequest_id1`(`yeZhuID`) USING BTREE,
  INDEX `FOREST_RepairRequest_id2`(`roomNameID`) USING BTREE,
  CONSTRAINT `FOREST_RepairRequest_id1` FOREIGN KEY (`yeZhuID`) REFERENCES `forest_yezhumessage` (`yezhuid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FOREST_RepairRequest_id2` FOREIGN KEY (`roomNameID`) REFERENCES `forest_roomnamemessage` (`roomnameid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_repairrequest
-- ----------------------------

-- ----------------------------
-- Table structure for forest_roommessage
-- ----------------------------
DROP TABLE IF EXISTS `forest_roommessage`;
CREATE TABLE `forest_roommessage`  (
  `floorID` int(11) NOT NULL AUTO_INCREMENT COMMENT '楼盘ID',
  `floorName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '楼盘名称',
  `floorDate` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '楼盘时间',
  `floorBeiZhu` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`floorID`) USING BTREE,
  INDEX `RoomID`(`floorID`) USING BTREE,
  INDEX `RoomID_2`(`floorID`) USING BTREE,
  INDEX `RoomID_3`(`floorID`) USING BTREE,
  INDEX `RoomID_4`(`floorID`) USING BTREE,
  INDEX `RoomID_5`(`floorID`) USING BTREE,
  INDEX `floorID`(`floorID`) USING BTREE,
  INDEX `floorID_2`(`floorID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_roommessage
-- ----------------------------
INSERT INTO `forest_roommessage` VALUES (1, '1号楼', '2020-10-24 16:36:15', NULL);
INSERT INTO `forest_roommessage` VALUES (2, '2号楼', '2020-10-24 16:45:15', NULL);
INSERT INTO `forest_roommessage` VALUES (3, '3号楼', '2020-10-24 16:55:15', NULL);
INSERT INTO `forest_roommessage` VALUES (4, '4号楼', '2020-10-24 16:35:15', NULL);

-- ----------------------------
-- Table structure for forest_roomname
-- ----------------------------
DROP TABLE IF EXISTS `forest_roomname`;
CREATE TABLE `forest_roomname`  (
  `roomID` int(11) NOT NULL AUTO_INCREMENT,
  `roomName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `roomDate` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `roomBeiZhu` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`roomID`) USING BTREE,
  INDEX `roomID`(`roomID`) USING BTREE,
  INDEX `roomID_2`(`roomID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_roomname
-- ----------------------------
INSERT INTO `forest_roomname` VALUES (1, '101', '2020-10-24 16:36:15', NULL);
INSERT INTO `forest_roomname` VALUES (2, '102', '2020-10-24 16:45:15', NULL);
INSERT INTO `forest_roomname` VALUES (3, '103', '2020-10-24 16:55:15', NULL);
INSERT INTO `forest_roomname` VALUES (4, '104', '2020-10-24 16:35:15', NULL);

-- ----------------------------
-- Table structure for forest_roomnamemessage
-- ----------------------------
DROP TABLE IF EXISTS `forest_roomnamemessage`;
CREATE TABLE `forest_roomnamemessage`  (
  `roomNameID` int(11) NOT NULL AUTO_INCREMENT COMMENT '关联ID',
  `yeZhuID` int(11) NULL DEFAULT NULL COMMENT '业主ID',
  `roomID` int(11) NULL DEFAULT NULL COMMENT '房间ID',
  `floorID` int(11) NULL DEFAULT NULL COMMENT '楼盘ID',
  `roomNameDate` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '关联时间',
  `roomState` int(11) NULL DEFAULT NULL COMMENT '房间状态（\r\n0  未入住\r\n1  已入住）',
  PRIMARY KEY (`roomNameID`) USING BTREE,
  INDEX `roomNameID`(`roomNameID`) USING BTREE,
  INDEX `forest_roomNameID2`(`roomID`) USING BTREE,
  INDEX `forest_roomNameID3`(`floorID`) USING BTREE,
  INDEX `forest_roomNameID1`(`yeZhuID`) USING BTREE,
  CONSTRAINT `forest_roomNameID1` FOREIGN KEY (`yeZhuID`) REFERENCES `forest_yezhumessage` (`yezhuid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `forest_roomNameID2` FOREIGN KEY (`roomID`) REFERENCES `forest_roomname` (`roomid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `forest_roomNameID3` FOREIGN KEY (`floorID`) REFERENCES `forest_roommessage` (`floorID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_roomnamemessage
-- ----------------------------
INSERT INTO `forest_roomnamemessage` VALUES (2, 1, 1, 2, '2020-10-24 16:46:15', 2);
INSERT INTO `forest_roomnamemessage` VALUES (3, 1, 2, 2, '2020-10-24 17:46:15', 2);
INSERT INTO `forest_roomnamemessage` VALUES (4, 40, 4, 4, '2020-10-25 14:27:38', 1);
INSERT INTO `forest_roomnamemessage` VALUES (5, 8, 4, 3, '2020-10-25 14:27:45', 1);
INSERT INTO `forest_roomnamemessage` VALUES (6, 9, 4, 2, '2020-10-25 14:32:10', 2);
INSERT INTO `forest_roomnamemessage` VALUES (7, 8, 1, 3, '2020-10-25 14:36:39', 2);
INSERT INTO `forest_roomnamemessage` VALUES (8, 8, 2, 3, '2020-10-25 15:55:17', 2);

-- ----------------------------
-- Table structure for forest_travelrecords
-- ----------------------------
DROP TABLE IF EXISTS `forest_travelrecords`;
CREATE TABLE `forest_travelrecords`  (
  `TravelRecoID` int(11) NOT NULL AUTO_INCREMENT COMMENT '出行记录ID',
  `TravelRecoPlates` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车牌照',
  `TravelRecoName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车主姓名',
  `TravelRecoStates` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '出行状态(\r\n0   出去\r\n1  回来)',
  `XtYongHuID` int(11) NULL DEFAULT NULL COMMENT '物业ID',
  `CarBeiZhu` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`TravelRecoID`) USING BTREE,
  INDEX `FOREST_TravelRecords_id`(`XtYongHuID`) USING BTREE,
  CONSTRAINT `FOREST_TravelRecords_id` FOREIGN KEY (`XtYongHuID`) REFERENCES `forest_xitongyonghu` (`xtYongHuID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_travelrecords
-- ----------------------------

-- ----------------------------
-- Table structure for forest_xitongyonghu
-- ----------------------------
DROP TABLE IF EXISTS `forest_xitongyonghu`;
CREATE TABLE `forest_xitongyonghu`  (
  `xtYongHuID` int(11) NOT NULL AUTO_INCREMENT COMMENT '系统用户ID',
  `xtYongUserName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '登陆名',
  `xtYongMiMa` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '用户密码',
  `xtYongName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '用户姓名',
  `xtYongHuSFZ` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '身份证号',
  `xtYongHuSex` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '性别',
  `xtYongHuEmail` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '电子邮件',
  `xtYongHuPhone` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '手机号',
  `xtYongHuFlower` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '楼层',
  `xtYongHuState` int(1) UNSIGNED ZEROFILL NULL DEFAULT 0 COMMENT '审核状态',
  `xtYongHuJiBie` int(11) NULL DEFAULT NULL COMMENT '用户级别\r\n(0-代表管理层\r\n1代表业务人员）',
  PRIMARY KEY (`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_2`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_3`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_4`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_5`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_6`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_7`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_8`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_9`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_10`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_11`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_12`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_13`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_14`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_15`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_16`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_17`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_18`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_19`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_20`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_21`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_22`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_23`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_24`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_25`(`xtYongHuID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_xitongyonghu
-- ----------------------------
INSERT INTO `forest_xitongyonghu` VALUES (1, 'root', '123456789', '王欢', '411526199908014517', '男', '508377132@qq.com', '15737626628', 0, 0, 2);
INSERT INTO `forest_xitongyonghu` VALUES (2, 'wanghuan1', '123123', '王欢', '411526199908014518', '男', '508377132@qq.com', '15737626628', 0, 0, 1);
INSERT INTO `forest_xitongyonghu` VALUES (16, 'wanghuan22', '123123', '王欢', '411529188803041245', '男', '508377132@qq.com', '15737626628', 0, 0, 1);

-- ----------------------------
-- Table structure for forest_yezhumessage
-- ----------------------------
DROP TABLE IF EXISTS `forest_yezhumessage`;
CREATE TABLE `forest_yezhumessage`  (
  `yeZhuID` int(11) NOT NULL AUTO_INCREMENT COMMENT '业主ID',
  `yeZhuName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '业主姓名',
  `yeZhuSex` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '性别',
  `yeZhuPhone` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '电话',
  `yeZhuSFZ` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '身份证',
  `yeZhuEmail` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '电子邮件',
  `yeZhuPassword` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '密码',
  `yeZhuTime` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '登记时间',
  `yeZhuUserName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '登录名',
  PRIMARY KEY (`yeZhuID`) USING BTREE,
  INDEX `YeZhuID`(`yeZhuID`) USING BTREE,
  INDEX `YeZhuID_2`(`yeZhuID`) USING BTREE,
  INDEX `YeZhuID_3`(`yeZhuID`) USING BTREE,
  INDEX `YeZhuID_4`(`yeZhuID`) USING BTREE,
  INDEX `YeZhuID_5`(`yeZhuID`) USING BTREE,
  INDEX `YeZhuID_6`(`yeZhuID`) USING BTREE,
  INDEX `YeZhuID_7`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_8`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_9`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_10`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_11`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_12`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_13`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_14`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_15`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_16`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_17`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_18`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_19`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_20`(`yeZhuID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 50 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_yezhumessage
-- ----------------------------
INSERT INTO `forest_yezhumessage` VALUES (1, '王欢', '男', '15737626628', '411526199908015417', '50231425@qq.com', NULL, '2020-10-24 16:35:15', NULL);
INSERT INTO `forest_yezhumessage` VALUES (5, '王喜', '女', '18956235623', '41152709054519', '1111111@qq.com', NULL, '2020-10-24 16:34:05', NULL);
INSERT INTO `forest_yezhumessage` VALUES (6, '王哈', '男', '15705673456', '411527189009094516', '2020202220@qq.com', '', '2020-10-24 16:31:42', '');
INSERT INTO `forest_yezhumessage` VALUES (7, '王哈喽', '女', '15785673456', '411527189008014516', '2020202120@qq.com', '', '2020-10-24 17:32:33', '');
INSERT INTO `forest_yezhumessage` VALUES (8, '王简介', '男', '15775673456', '411527189008024516', '2020222020@qq.com', '', '2020-10-24 17:32:12', '');
INSERT INTO `forest_yezhumessage` VALUES (9, '王哈醒', '男', '15755673456', '411527189008044516', '2025202020@qq.com', '', '2020-10-24 16:27:17', '');
INSERT INTO `forest_yezhumessage` VALUES (10, '王哈哈', '男', '15735673456', '411527189005094516', '2025620200@qq.com', '', '2020-10-24 16:25:04', '');
INSERT INTO `forest_yezhumessage` VALUES (33, '万欢', '男', '15737626628', '411527190505064516', '508377132@qq.com', NULL, '2020-10-24 16:37:34', NULL);
INSERT INTO `forest_yezhumessage` VALUES (34, '李端', '男', '15737626689', '411526177706054519', '15737626689@qq.com', NULL, '2020-10-24 19:11:36', NULL);
INSERT INTO `forest_yezhumessage` VALUES (35, '李雪', '女', '15737626678', '411526177706054520', '15737626678@qq.com', NULL, '2020-10-24 19:12:23', NULL);
INSERT INTO `forest_yezhumessage` VALUES (36, '李波', '男', '15737626675', '411526177707054187', '15737626675@qq.com', NULL, '2020-10-24 19:12:48', NULL);
INSERT INTO `forest_yezhumessage` VALUES (37, '李博', '男', '18737626622', '411526177707052211', '18737626622@qq.com', NULL, '2020-10-24 19:13:20', NULL);
INSERT INTO `forest_yezhumessage` VALUES (38, '张旭', '男', '18737626621', '411526177707052222', '18737626621@qq.com', NULL, '2020-10-24 19:13:41', NULL);
INSERT INTO `forest_yezhumessage` VALUES (39, '张天禧', '女', '18737624517', '411526177701014125', '18737624517@qq.com', NULL, '2020-10-24 20:56:27', NULL);
INSERT INTO `forest_yezhumessage` VALUES (40, '张天下', '男', '18737624514', '411526177701014122', '18737624514@qq.com', NULL, '2020-10-24 19:14:46', NULL);
INSERT INTO `forest_yezhumessage` VALUES (41, '周深', '男', '18752034514', '411526177701055203', '18752034514@qq.com', NULL, '2020-10-24 19:15:36', NULL);
INSERT INTO `forest_yezhumessage` VALUES (42, '周空', '男', '18752034512', '411526177701055202', '18752034512@qq.com', NULL, '2020-10-24 19:15:52', NULL);
INSERT INTO `forest_yezhumessage` VALUES (43, '王虎', '男', '13525686852', '413025199905024519', '13525686852@qq.com', NULL, '2020-10-24 19:20:43', NULL);
INSERT INTO `forest_yezhumessage` VALUES (45, '王新华', '男', '13465285268', '485258166605024513', '13465285268@qq.com', NULL, '2020-10-24 21:15:52', NULL);
INSERT INTO `forest_yezhumessage` VALUES (46, '李海青', '男', '15737668889', '485258166605024556', '15737668889@qq.com', NULL, '2020-10-24 21:21:25', NULL);
INSERT INTO `forest_yezhumessage` VALUES (47, '万欢', '男', '15737626628', '452152199908044516', '508377132@qq.com', NULL, '2020-10-24 22:21:17', NULL);
INSERT INTO `forest_yezhumessage` VALUES (48, '王特特', '男', '15737626628', '452123199906054517', '508377132@qq.com', NULL, '2020-10-24 22:29:35', NULL);

SET FOREIGN_KEY_CHECKS = 1;
