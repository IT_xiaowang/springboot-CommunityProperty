/*
 Navicat Premium Data Transfer

 Source Server         : wanghuan
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : db_forest_village

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 22/10/2020 21:08:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for forest_carmessage
-- ----------------------------
DROP TABLE IF EXISTS `forest_carmessage`;
CREATE TABLE `forest_carmessage`  (
  `carID` int(11) NOT NULL AUTO_INCREMENT COMMENT '车位ID',
  `carArea` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车位区域（楼号）',
  `carName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车位编号',
  `carImage` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车辆照片',
  `carPlates` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车牌照',
  `carStates` int(255) NULL DEFAULT NULL COMMENT '车位状态(\r\n0  车位未售出\r\n1  车位已售出)',
  `yeZhuID` int(11) NULL DEFAULT NULL COMMENT '业主ID',
  `carBeiZhu` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`carID`) USING BTREE,
  INDEX `yeZhuID_id`(`yeZhuID`) USING BTREE,
  CONSTRAINT `yeZhuID_id` FOREIGN KEY (`yeZhuID`) REFERENCES `forest_yezhumessage` (`yezhuid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_carmessage
-- ----------------------------

-- ----------------------------
-- Table structure for forest_complaint
-- ----------------------------
DROP TABLE IF EXISTS `forest_complaint`;
CREATE TABLE `forest_complaint`  (
  `comID` int(11) NOT NULL AUTO_INCREMENT COMMENT '投诉ID',
  `yeZhuID` int(11) NULL DEFAULT NULL COMMENT '业主ID',
  `roomNameID` int(11) NULL DEFAULT NULL COMMENT '房间ID',
  `comTitle` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '投诉标题',
  `comContent` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '投诉内容',
  `comMatter` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '投诉类别',
  `comState` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '处理状态\r\n(0 未处理\r\n1 正在处理\r\n2 处理完成)',
  `comAssess` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '住户评价',
  `comDate` datetime(0) NULL DEFAULT NULL COMMENT '投诉时间',
  PRIMARY KEY (`comID`) USING BTREE,
  INDEX `FOREST_Complaint_id2`(`roomNameID`) USING BTREE,
  INDEX `FOREST_Complaint_id1`(`yeZhuID`) USING BTREE,
  CONSTRAINT `FOREST_Complaint_id1` FOREIGN KEY (`yeZhuID`) REFERENCES `forest_yezhumessage` (`yezhuid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FOREST_Complaint_id2` FOREIGN KEY (`roomNameID`) REFERENCES `forest_roomnamemessage` (`roomnameid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_complaint
-- ----------------------------

-- ----------------------------
-- Table structure for forest_currententry
-- ----------------------------
DROP TABLE IF EXISTS `forest_currententry`;
CREATE TABLE `forest_currententry`  (
  `currentEntryID` int(11) NOT NULL AUTO_INCREMENT COMMENT '日常记录ID',
  `currentEntryName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '日常名称（姓名）',
  `xtYongHuID` int(11) NULL DEFAULT NULL COMMENT '物业ID',
  `yeZhuID` int(11) NULL DEFAULT NULL COMMENT '业主ID',
  `styleID` int(11) NULL DEFAULT NULL COMMENT '类型ID',
  `currentEntryBeiZhu` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`currentEntryID`) USING BTREE,
  INDEX `FOREST_currentry_id1`(`xtYongHuID`) USING BTREE,
  INDEX `FOREST_currentry_id2`(`yeZhuID`) USING BTREE,
  INDEX `FOREST_currentry_id3`(`styleID`) USING BTREE,
  CONSTRAINT `FOREST_currentry_id1` FOREIGN KEY (`xtYongHuID`) REFERENCES `forest_xitongyonghu` (`xtyonghuid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FOREST_currentry_id2` FOREIGN KEY (`yeZhuID`) REFERENCES `forest_yezhumessage` (`yezhuid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FOREST_currentry_id3` FOREIGN KEY (`styleID`) REFERENCES `forest_currententrystyle` (`styleid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_currententry
-- ----------------------------

-- ----------------------------
-- Table structure for forest_currententrystyle
-- ----------------------------
DROP TABLE IF EXISTS `forest_currententrystyle`;
CREATE TABLE `forest_currententrystyle`  (
  `styleID` int(11) NOT NULL AUTO_INCREMENT COMMENT '类型ID',
  `styleName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '操作类型',
  `styleBeiZhu` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`styleID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_currententrystyle
-- ----------------------------

-- ----------------------------
-- Table structure for forest_ordermanage
-- ----------------------------
DROP TABLE IF EXISTS `forest_ordermanage`;
CREATE TABLE `forest_ordermanage`  (
  `orderID` int(11) NOT NULL AUTO_INCREMENT COMMENT '收费单ID',
  `projectID` int(11) NOT NULL COMMENT '收费项ID',
  `orderCharge` double NULL DEFAULT NULL COMMENT '单价',
  `orderChargeActual` double NULL DEFAULT NULL COMMENT '收费价格',
  `orderState` int(255) NULL DEFAULT NULL COMMENT '	收费状态（\r\n1、已收费\r\n0、未收费\r\n）',
  `orderDate` datetime(0) NULL DEFAULT NULL COMMENT '订单时间',
  `yeZhuID` int(11) NULL DEFAULT NULL COMMENT '业主ID',
  `roomNameID` int(11) NULL DEFAULT NULL COMMENT '关联ID',
  PRIMARY KEY (`orderID`) USING BTREE,
  INDEX `forest_ordermessage01`(`projectID`) USING BTREE,
  INDEX `forest_ordermessage02`(`yeZhuID`) USING BTREE,
  INDEX `forest_ordermessage03`(`roomNameID`) USING BTREE,
  CONSTRAINT `forest_ordermessage01` FOREIGN KEY (`projectID`) REFERENCES `forest_projectmanage` (`projectid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `forest_ordermessage02` FOREIGN KEY (`yeZhuID`) REFERENCES `forest_yezhumessage` (`yezhuid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `forest_ordermessage03` FOREIGN KEY (`roomNameID`) REFERENCES `forest_roomnamemessage` (`roomnameid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_ordermanage
-- ----------------------------

-- ----------------------------
-- Table structure for forest_projectmanage
-- ----------------------------
DROP TABLE IF EXISTS `forest_projectmanage`;
CREATE TABLE `forest_projectmanage`  (
  `projectID` int(11) NOT NULL AUTO_INCREMENT COMMENT '收费项ID',
  `projectName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '收费项名称',
  `projectPrice` decimal(10, 2) NULL DEFAULT NULL COMMENT '单价',
  PRIMARY KEY (`projectID`) USING BTREE,
  INDEX `ProjectID`(`projectID`) USING BTREE,
  INDEX `ProjectID_2`(`projectID`) USING BTREE,
  INDEX `ProjectID_3`(`projectID`) USING BTREE,
  INDEX `ProjectID_4`(`projectID`) USING BTREE,
  INDEX `ProjectID_5`(`projectID`) USING BTREE,
  INDEX `ProjectID_6`(`projectID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_projectmanage
-- ----------------------------

-- ----------------------------
-- Table structure for forest_repairrequest
-- ----------------------------
DROP TABLE IF EXISTS `forest_repairrequest`;
CREATE TABLE `forest_repairrequest`  (
  `repairID` int(11) NOT NULL AUTO_INCREMENT COMMENT '维修ID',
  `yeZhuID` int(11) NULL DEFAULT NULL COMMENT '业主ID',
  `shenQingShiJian` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '申请时间',
  `repairMessage` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '维修项目',
  `repairState` int(11) NULL DEFAULT NULL COMMENT '维修状态\r\n(0 未处理\r\n1 正在处理\r\n2 处理完成)',
  `repairDetail` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '维修详情',
  `roomNameID` int(11) NULL DEFAULT NULL COMMENT '关联ID',
  PRIMARY KEY (`repairID`) USING BTREE,
  INDEX `FOREST_RepairRequest_id1`(`yeZhuID`) USING BTREE,
  INDEX `FOREST_RepairRequest_id2`(`roomNameID`) USING BTREE,
  CONSTRAINT `FOREST_RepairRequest_id1` FOREIGN KEY (`yeZhuID`) REFERENCES `forest_yezhumessage` (`yezhuid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FOREST_RepairRequest_id2` FOREIGN KEY (`roomNameID`) REFERENCES `forest_roomnamemessage` (`roomnameid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_repairrequest
-- ----------------------------

-- ----------------------------
-- Table structure for forest_roommessage
-- ----------------------------
DROP TABLE IF EXISTS `forest_roommessage`;
CREATE TABLE `forest_roommessage`  (
  `floorID` int(11) NOT NULL AUTO_INCREMENT COMMENT '楼盘ID',
  `floorName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '楼盘名称',
  `foorBeiZhu` int(11) NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`floorID`) USING BTREE,
  INDEX `RoomID`(`floorID`) USING BTREE,
  INDEX `RoomID_2`(`floorID`) USING BTREE,
  INDEX `RoomID_3`(`floorID`) USING BTREE,
  INDEX `RoomID_4`(`floorID`) USING BTREE,
  INDEX `RoomID_5`(`floorID`) USING BTREE,
  INDEX `floorID`(`floorID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_roommessage
-- ----------------------------

-- ----------------------------
-- Table structure for forest_roomname
-- ----------------------------
DROP TABLE IF EXISTS `forest_roomname`;
CREATE TABLE `forest_roomname`  (
  `roomID` int(11) NOT NULL AUTO_INCREMENT,
  `roomName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `roomBeiZhu` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`roomID`) USING BTREE,
  INDEX `roomID`(`roomID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_roomname
-- ----------------------------

-- ----------------------------
-- Table structure for forest_roomnamemessage
-- ----------------------------
DROP TABLE IF EXISTS `forest_roomnamemessage`;
CREATE TABLE `forest_roomnamemessage`  (
  `roomNameID` int(11) NOT NULL AUTO_INCREMENT COMMENT '关联ID',
  `yeZhuID` int(11) NULL DEFAULT NULL COMMENT '业主ID',
  `roomID` int(11) NULL DEFAULT NULL COMMENT '房间ID',
  `floorID` int(11) NULL DEFAULT NULL COMMENT '楼盘ID',
  `roomState` int(11) NULL DEFAULT NULL COMMENT '房间状态（\r\n0  未入住\r\n1  已入住）',
  PRIMARY KEY (`roomNameID`) USING BTREE,
  INDEX `roomNameID`(`roomNameID`) USING BTREE,
  INDEX `forest_roomNameID1`(`yeZhuID`) USING BTREE,
  INDEX `forest_roomNameID2`(`roomID`) USING BTREE,
  INDEX `forest_roomNameID3`(`floorID`) USING BTREE,
  CONSTRAINT `forest_roomNameID1` FOREIGN KEY (`yeZhuID`) REFERENCES `forest_yezhumessage` (`yezhuid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `forest_roomNameID2` FOREIGN KEY (`roomID`) REFERENCES `forest_roomname` (`roomid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `forest_roomNameID3` FOREIGN KEY (`floorID`) REFERENCES `forest_roommessage` (`floorid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_roomnamemessage
-- ----------------------------

-- ----------------------------
-- Table structure for forest_travelrecords
-- ----------------------------
DROP TABLE IF EXISTS `forest_travelrecords`;
CREATE TABLE `forest_travelrecords`  (
  `TravelRecoID` int(11) NOT NULL AUTO_INCREMENT COMMENT '出行记录ID',
  `TravelRecoPlates` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车牌照',
  `TravelRecoName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '车主姓名',
  `TravelRecoStates` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '出行状态(\r\n0   出去\r\n1  回来)',
  `XtYongHuID` int(11) NULL DEFAULT NULL COMMENT '物业ID',
  `CarBeiZhu` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`TravelRecoID`) USING BTREE,
  INDEX `FOREST_TravelRecords_id`(`XtYongHuID`) USING BTREE,
  CONSTRAINT `FOREST_TravelRecords_id` FOREIGN KEY (`XtYongHuID`) REFERENCES `forest_xitongyonghu` (`xtYongHuID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_travelrecords
-- ----------------------------

-- ----------------------------
-- Table structure for forest_xitongyonghu
-- ----------------------------
DROP TABLE IF EXISTS `forest_xitongyonghu`;
CREATE TABLE `forest_xitongyonghu`  (
  `xtYongHuID` int(11) NOT NULL AUTO_INCREMENT COMMENT '系统用户ID',
  `xtYongUserName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '登陆名',
  `xtYongMiMa` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '用户密码',
  `xtYongName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '用户姓名',
  `xtYongHuSFZ` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '身份证号',
  `xtYongHuSex` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '性别',
  `xtYongHuEmail` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '电子邮件',
  `xtYongHuPhone` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '手机号',
  `xtYongHuFlower` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '楼层',
  `xtYongHuState` int(1) UNSIGNED ZEROFILL NULL DEFAULT 0 COMMENT '审核状态',
  `xtYongHuJiBie` int(11) NULL DEFAULT NULL COMMENT '用户级别\r\n(0-代表管理层\r\n1代表业务人员）',
  PRIMARY KEY (`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_2`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_3`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_4`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_5`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_6`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_7`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_8`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_9`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_10`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_11`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_12`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_13`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_14`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_15`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_16`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_17`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_18`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_19`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_20`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_21`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_22`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_23`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_24`(`xtYongHuID`) USING BTREE,
  INDEX `XtYongHuID_25`(`xtYongHuID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_xitongyonghu
-- ----------------------------
INSERT INTO `forest_xitongyonghu` VALUES (1, 'root', '123456789', '王欢', '411526199908014517', '男', '508377132@qq.com', '15737626628', 0, 0, 2);
INSERT INTO `forest_xitongyonghu` VALUES (2, 'wanghuan1', '123123', '王欢', '411526199908014518', '男', '508377132@qq.com', '15737626628', 0, 0, 1);
INSERT INTO `forest_xitongyonghu` VALUES (16, 'wanghuan22', '123123', '王欢', '411529188803041245', '男', '508377132@qq.com', '15737626628', 0, 0, 1);

-- ----------------------------
-- Table structure for forest_yezhumessage
-- ----------------------------
DROP TABLE IF EXISTS `forest_yezhumessage`;
CREATE TABLE `forest_yezhumessage`  (
  `yeZhuID` int(11) NOT NULL AUTO_INCREMENT COMMENT '业主ID',
  `yeZhuName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '业主姓名',
  `yeZhuSex` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '性别',
  `yeZhuPhone` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '电话',
  `yeZhuSFZ` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '身份证',
  `yeZhuEmail` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '电子邮件',
  `yeZhuPassword` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '密码',
  `yeZhuTime` datetime(0) NULL DEFAULT NULL COMMENT '登记时间',
  `yeZhuUserName` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '登录名',
  PRIMARY KEY (`yeZhuID`) USING BTREE,
  INDEX `YeZhuID`(`yeZhuID`) USING BTREE,
  INDEX `YeZhuID_2`(`yeZhuID`) USING BTREE,
  INDEX `YeZhuID_3`(`yeZhuID`) USING BTREE,
  INDEX `YeZhuID_4`(`yeZhuID`) USING BTREE,
  INDEX `YeZhuID_5`(`yeZhuID`) USING BTREE,
  INDEX `YeZhuID_6`(`yeZhuID`) USING BTREE,
  INDEX `YeZhuID_7`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_8`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_9`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_10`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_11`(`yeZhuID`) USING BTREE,
  INDEX `yeZhuID_12`(`yeZhuID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of forest_yezhumessage
-- ----------------------------
INSERT INTO `forest_yezhumessage` VALUES (1, '王欢', '男', '15737626628', '411526199908015417', '50231425@qq.com', NULL, NULL, NULL);
INSERT INTO `forest_yezhumessage` VALUES (2, '王鑫', '女', '15736255628', '411526198205014517', '55555555@qq.com', NULL, NULL, NULL);
INSERT INTO `forest_yezhumessage` VALUES (3, '王菲菲', '女', '15896325642', '411529183205014519', '66666666@163.com', NULL, NULL, NULL);
INSERT INTO `forest_yezhumessage` VALUES (4, '王海', '男', '15963254568', '411523199907054512', '9999999@qq.com', NULL, NULL, NULL);
INSERT INTO `forest_yezhumessage` VALUES (5, '王喜', '男', '18956235623', '41152709054519', '1111111@qq.com', NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
